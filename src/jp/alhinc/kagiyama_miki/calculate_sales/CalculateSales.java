package jp.alhinc.kagiyama_miki.calculate_sales;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {

	public static void main(String[] args) {
		System.out.println("ここにあるファイルを開きます => " + args[0]);
		BufferedReader br = null ;
		HashMap<String, String> map = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

		try {
			File file = new File(args[0] , "branch.lst") ;
			if(file.exists()) {
				FileReader fr = new FileReader(file) ;
				br = new BufferedReader(fr) ;
				String line ;

				while((line = br.readLine()) != null){
					String[] str = line.split(",");
					if(str.length == 2) {
						if(str[0].matches("^[0-9]{3}")) {
							map.put(str[0], str[1]);
							salesMap.put(str[0], 0l);
							//フォーマットが不正の場合
							} else {
							System.out.println("支店定義ファイルのフォーマットが不正です");
							return ;
						}
					} else {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return ;
					}
				}
			//ファイルが見つからなかった場合
			} else {
				System.out.println("支店定義ファイルが存在しません");
				return ;
			}

			//その他のエラー
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました") ;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					;
				}
			}
		}


		File record = new File(args[0]) ;
		File records[] = record.listFiles() ;

		List<Integer> countList = new ArrayList<>();
		int n = 0 ; //リストの呼び出しに使用
		for(int i = 0 ; i < records.length ; i++) {
			if(records[i].getName().matches("^[0-9]{8}.rcd$")) {
				countList.add(Integer.parseInt(records[i].getName().substring(0 , 8)) );
			} // ファイル名の数字のみをリストへ追加
		}
		int x = countList.size() - 1; //ループ回数はファイル数より1少なくする
		for(int i = 0 ; i < x ; i++) {
			if(countList.get(n) + 1 != countList.get(n + 1)) {
				System.out.println("売上ファイル名が連番になっていません");
				return ;
			} //呼び出したリストに1足した数とリスト呼出に使用した数字を1足した数に相違があればエラー文を返す
			n = n + 1; //呼び出す数字を1加算し、次のリストのチェック
		}

		for(int i = 0 ; i < records.length ; i++) {
			if(records[i].getName().matches("^[0-9]{8}.rcd$")) {
				List<String> list = new ArrayList<>();
				BufferedReader bufferedReader = null;

				try {
					FileReader fileReader = new FileReader(records[i]);
					bufferedReader = new BufferedReader(fileReader);
					String data ;

					while((data = bufferedReader.readLine()) != null) {
						list.add(data) ;
					}



					if(salesMap.containsKey(list.get(0))) {
						int lineNumber = 0; //ファイルの行数をカウント
						LineNumberReader lnr = new LineNumberReader(bufferedReader);
						while (lnr.readLine() != null){
							lineNumber++;
						}
						if(lineNumber >= 3) {
							System.out.println(records[i].getName() + "のフォーマットが不正です"); //3行以上だった場合、エラー文を表示
							return ;
						}
							Long result = salesMap.get(list.get(0));
							result += Long.parseLong(list.get(1));

							String number = result + ""; //桁数をカウントするためにString型へ代入
							if(number.length() > 10){
								System.out.println("合計金額が10桁を超えました"); //10桁以上ならエラー文を表示
								return ;
							}
							salesMap.put(list.get(0) , result) ;
							
					} else {
						System.out.println(records[i].getName() + "の支店コードが不正です");
						return ;
					}

				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				} finally {
					if(bufferedReader != null) {
						try {
								bufferedReader.close();
							} catch (IOException e) {
							;
						}
					}
				}
			}
		}

		BufferedWriter bw = null;
		try {
			File rfile = new File(args[0] , "branch.out" ) ;
			FileWriter fw = new FileWriter(rfile , true) ;
			bw = new BufferedWriter(fw) ;
			for (String key : salesMap.keySet()) {
				bw.write(key + "," + map.get(key) + "," + salesMap.get(key));
				bw.newLine() ;
			}
		}
		catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
		}
		finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					;
				}
			}
		}
	}
}
